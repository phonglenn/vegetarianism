//
//  itemReference.m
//  ReligiousLife
//
//  Created by harveynash on 4/22/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "itemReference.h"

@implementation itemReference
@synthesize leftColum;
@synthesize imageView;
@synthesize backgroundLabel;
@synthesize titleItem;
@synthesize customButton;

#define WIDTH_ITEM 100
#define HEIGHT_ITEM 100
#define WIDTH_COLUM 5
#define HEIGHT_BACKGROUND_LABEL 25
#define HEIGHT_TITLE_ITEM 14
#define WIDTH_SPACE 2
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        leftColum = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_COLUM, HEIGHT_ITEM)];
        leftColum.backgroundColor = [UIColor yellowColor];
        leftColum.layer.cornerRadius = 4.0f;
        [self addSubview:leftColum];
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH_COLUM + WIDTH_SPACE, 0, self.frame.size.width - WIDTH_COLUM - WIDTH_SPACE, HEIGHT_ITEM)];
        imageView.image = [UIImage imageNamed:@"homescreen"];
        imageView.layer.cornerRadius = 7.0f;
        imageView.layer.masksToBounds = YES;
        imageView.layer.borderWidth = 1;
        [imageView.layer setBorderColor:[[UIColor whiteColor] CGColor]];
        [self addSubview:imageView];
        
        backgroundLabel = [[UIView alloc] initWithFrame:CGRectMake(WIDTH_COLUM + WIDTH_SPACE + 1, self.frame.size.height - HEIGHT_BACKGROUND_LABEL, self.frame.size.width - WIDTH_COLUM - WIDTH_SPACE -2, HEIGHT_BACKGROUND_LABEL)];
        backgroundLabel.layer.cornerRadius = 8.0f;
        backgroundLabel.backgroundColor = [UIColor whiteColor];
        [backgroundLabel setAlpha:0.5f];
        [self addSubview:backgroundLabel];
        
        titleItem = [[UILabel alloc] initWithFrame:CGRectMake(backgroundLabel.frame.origin.x + 2, backgroundLabel.frame.origin.y + (backgroundLabel.frame.size.height - HEIGHT_TITLE_ITEM)/2, backgroundLabel.frame.size.width - 4, HEIGHT_TITLE_ITEM)];
        titleItem.font = [UIFont fontWithName:@"UTM_Flamenco" size:10];
        titleItem.textColor = [UIColor brownColor];
        [self addSubview:titleItem];
        
        customButton = [UIButton buttonWithType:UIButtonTypeCustom];
        customButton.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [self addSubview:customButton];
    }
    return self;
}
-(void)setTitleAndImage:(NSString *)title image:(UIImage *)image tag:(int)tag;
{
    if(title)
    {
        self.titleItem.text = title;
    }
    if(image)
    {
        self.imageView.image = image;
    }
    if(tag)
    {
        self.customButton.tag = tag;
    }
}

@end
