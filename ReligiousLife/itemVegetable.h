//
//  itemVegetable.h
//  ReligiousLife
//
//  Created by harveynash on 4/23/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface itemVegetable : UIView
@property (strong,nonatomic) UIView *leftColum;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIView *backgroundLabel;
@property (strong, nonatomic) UILabel *titleItem;
@property (strong, nonatomic) UIButton *customButton;
-(void)setTitleAndImage:(NSString *)title image:(UIImage *)image tag:(int)tag;
@end
