//
//  JSONHelper.h
//  Halalgems
//
//  Created by Nguyen Minh Khoai on 12/20/12.
//  Copyright (c) 2012 Nguyen The Phu. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JSONHelper : NSObject

+ (BOOL)isSuccessfulRequest:(NSDictionary *)dict;


+ (void)getListOfReferenceFromData:(NSDictionary *)dict
                    onCompleteBlock:(void (^)(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset))onCompeleteBlock;

+ (void)getListOfVegetableFromData:(NSDictionary *)dict
                   onCompleteBlock:(void (^)(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset))onCompeleteBlock;


@end
