/*============================================================================
 PROJECT: Halalgems
 FILE:    BaseNetwork.m
 AUTHOR:  Nguyen Quang Khai
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/

#import "BaseNetwork.h"

/*============================================================================
 MACRO
 =============================================================================*/

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
/*----------------------------------------------------------------------------
 Interface:   BaseNetwork
 -----------------------------------------------------------------------------*/
@interface BaseNetwork ()

@end

@implementation BaseNetwork
@synthesize networkEngine;
@synthesize enableDebugLog;

/*----------------------------------------------------------------------------
 Method:      init
 init function
 -----------------------------------------------------------------------------*/
- (id)init {
    self = [super init];
    if (self) {
    }

    return self;
}

- (id)initWithBaseURL:(NSString *)baseUrlString {
    self = [super init];
    if (self) {
        self.networkEngine = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseUrlString]];
        self.networkEngine.allowsInvalidSSLCertificate = YES;
    }

    return self;

}

/*----------------------------------------------------------------------------
 Method: service
 called to return a new service.
 -----------------------------------------------------------------------------*/
+ (BaseNetwork *)service {
    static BaseNetwork *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[BaseNetwork alloc] initWithBaseURL:kBaseService];
    });
    return sharedService;
}

/*----------------------------------------------------------------------------
 Method: requestWithURL: params: successBlock: errorBlock: httpMethod:
 called to get data with path and params.
 -----------------------------------------------------------------------------*/
- (void)requestWithURL:(NSString *)inputUrlString
                params:(NSMutableDictionary *)params
          successBlock:(void (^)(NSDictionary *))successBlock
            errorBlock:(void (^)(NSString *))errorBlock
            httpMethod:(NSString *)method {

//    if (!appDelegate.hasNetwork) {
//        errorBlock(kNoConnectionErrorMessage);
//        return;
//    }

    if ([method isEqualToString:@"GET"]) {

        [self.networkEngine getPath:inputUrlString
                         parameters:params
                            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                /* check dict respone */
                                NSDictionary *response = [operation.responseString JSONValue];
                                successBlock(response);
                                /* get URL string */
                                NSString *urlString = operation.request.URL.absoluteString;

                                /* check dict respone */
                                //DLog(@"Response for:%@\n==================\n%@==================", urlString, operation.responseString);

                            }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                /* get URL string */
                                NSString *urlString = operation.request.URL.absoluteString;

                                /* check dict respone */
                                //DLog(@"Response for:%@\n==================\n%@==================", urlString, operation.responseString);
                                NSString *errorMessage = operation.responseString;
                                if (!errorMessage) {
                                    errorMessage = kServiceNotAvailableMessage;
                                }
                                errorBlock(errorMessage);
                            }];
    } else if ([method isEqualToString:@"PUT"]) {
        [self.networkEngine putPath:inputUrlString
                         parameters:params
                            success:^(AFHTTPRequestOperation *operation, id jsonResponse) {
                                /* check dict respone */
                                NSDictionary *response = [operation.responseString JSONValue];
                                successBlock(response);

                            }
                            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                NSString *errorMessage = operation.responseString;
                                if (!errorMessage) {
                                    errorMessage = kServiceNotAvailableMessage;
                                }
                                errorBlock(errorMessage);
                            }];
    } else {
        [self.networkEngine postPath:inputUrlString
                          parameters:params
                             success:^(AFHTTPRequestOperation *operation, id jsonResponse) {
                                 /* check dict respone */
                                 NSDictionary *response = [operation.responseString JSONValue];
                                 successBlock(response);

                                 /* get URL string */
                                 NSString *urlString = operation.request.URL.absoluteString;

                                 /* check dict respone */
                                 //DLog(@"Response for:%@\n==================\n%@==================", urlString, operation.responseString);
                             }
                             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                 /* get URL string */
                                 NSString *urlString = operation.request.URL.absoluteString;

                                 /* check dict respone */
                                 //DLog(@"Response for:%@\n==================\n%@==================", urlString, operation.responseString);
                                 NSString *errorMessage = operation.responseString;
                                 if (!errorMessage) {
                                     errorMessage = kServiceNotAvailableMessage;
                                 }
                                 errorBlock(errorMessage);
                             }];
    }
}

/*----------------------------------------------------------------------------
 Method: requestWithHttpBody: headerFields: successBlock: errorBlock: httpMethod:
 called a request in SOAP type
 -----------------------------------------------------------------------------*/
- (void)requestWithHttpBody:(NSString *)httpBody
               headerFields:(NSMutableDictionary *)fields
               successBlock:(void (^)(id))successBlock
                 errorBlock:(void (^)(NSString *))errorBlock
                 httpMethod:(NSString *)method {

//    if (!appDelegate.hasNetwork) {
//        errorBlock(kNoConnectionErrorMessage);
//        return;
//    }

    /* create request */
    NSString *length = [NSString stringWithFormat:@"%d", httpBody.length];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kBaseService]];
    [request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];

    /* additional fields */
    for (NSString *key in fields.allKeys) {
        [request addValue:[fields valueForKey:key] forHTTPHeaderField:key];
    }

    [request setHTTPMethod:method];
    [request setHTTPBody:[httpBody dataUsingEncoding:NSUTF8StringEncoding]];

    /* add to queue */
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock(responseObject);
    }                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *errorMessage = operation.responseString;
        if (!errorMessage) {
            errorMessage = kServiceNotAvailableMessage;
        }
        errorBlock(errorMessage);
    }];

    [self.networkEngine.operationQueue addOperation:operation];
}

/*----------------------------------------------------------------------------
 Method: formRequestWithURLPath: params: filePath: successBlock: errorBlock: httpMethod:
 called a form request in SOAP type
 -----------------------------------------------------------------------------*/
- (void)formRequestWithURLPath:(NSString *)urlString
                        params:(NSMutableDictionary *)params
                      filePath:(NSString *)filePath
                          name:(NSString *)name
                  successBlock:(void (^)(NSDictionary *))successBlock
                    errorBlock:(void (^)(NSString *))errorBlock {

//    if (!appDelegate.hasNetwork) {
//        errorBlock(kNoConnectionErrorMessage);
//        return;
//    }

    /* create request path & call POST method */
    NSMutableURLRequest *request =
            [self.networkEngine multipartFormRequestWithMethod:@"POST"
                                                          path:urlString
                                                    parameters:params
                                     constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
                                         if (filePath.length > 0) {
                                             [formData appendPartWithFileURL:[NSURL fileURLWithPath:filePath]
                                                                        name:@"image"
                                                                       error:nil];
                                         }
                                     }];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    // if you want progress updates as it's uploading, uncomment the following:
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {

    }];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        successBlock(responseObject);
    }                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *errorMessage = error.localizedDescription;
        errorBlock(errorMessage);
    }];

    [networkEngine enqueueHTTPRequestOperation:operation];
}

/*----------------------------------------------------------------------------
 Method: formRequestWithURLPath: params: filePath: successBlock: errorBlock: httpMethod:
 called a form request in SOAP type
 -----------------------------------------------------------------------------*/
- (AFHTTPRequestOperation *)formRequestWithURLPath:(NSString *)urlString
                                            params:(NSMutableDictionary *)params
                                             image:(UIImage *)image
                                      successBlock:(void (^)(NSDictionary *))successBlock
                                        errorBlock:(void (^)(NSString *))errorBlock
                                     progressBlock:(void (^)(long long totalBytesWritten, long long totalBytesExpectedToWrite))progressBlock {

//    if (!appDelegate.hasNetwork) {
//        errorBlock(kNoConnectionErrorMessage);
//        return nil;
//    }

    NSData *data = UIImagePNGRepresentation(image);
    NSMutableURLRequest *request =
            [networkEngine multipartFormRequestWithMethod:@"POST"
                                                     path:urlString
                                               parameters:params
                                constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
                                    if (image && data.length > 0) {
                                        [formData appendPartWithFileData:data
                                                                    name:@"file"
                                                                fileName:[params objectForKey:@"name"]
                                                                mimeType:@"image/jpeg"];
                                    }
                                }];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        progressBlock(totalBytesWritten, totalBytesExpectedToWrite);
    }];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *response = [operation.responseString JSONValue];
        successBlock(response);
    }                                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString *errorMessage = error.localizedDescription;
        errorBlock(errorMessage);
    }];

    //continue upload when app go to background
    [operation setShouldExecuteAsBackgroundTaskWithExpirationHandler:^{
        // Handle iOS shutting you down (possibly make a note of where you
        // stopped so you can resume later)
    }];

    [self.networkEngine enqueueHTTPRequestOperation:operation];

    return operation;
}

/*----------------------------------------------------------------------------
 Method: formRequestWithURLPath: params: successBlock: errorBlock: httpMethod:
 called a form request in SOAP type
 -----------------------------------------------------------------------------*/
- (void)formRequestWithURLPath:(NSString *)urlString
                        params:(NSMutableDictionary *)params
                  successBlock:(void (^)(NSDictionary *))successBlock
                    errorBlock:(void (^)(NSString *))errorBlock {
    [self formRequestWithURLPath:urlString
                          params:params
                        filePath:nil
                            name:nil
                    successBlock:successBlock
                      errorBlock:errorBlock];
}

@end
