//
//  DetailRefReligion.h
//  ReligiousLife
//
//  Created by harveynash on 3/12/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailRefReligion : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *itemImage;
@property (strong, nonatomic) IBOutlet UILabel *materialLabel;

@property (strong, nonatomic) IBOutlet UITextView *materialTextView;
@property (strong, nonatomic) IBOutlet UILabel *processLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *processScrollView;
@property (strong, nonatomic) IBOutlet UITextView *processTextView;
@property (nonatomic, strong) NSArray *imageArray;
@end
