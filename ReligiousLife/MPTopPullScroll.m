//
//  MPTopPullScroll.m
//  MobionPhoto
//
//  Created by Han Korea on 11/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MPTopPullScroll.h"

#define kNavHeight  320

@interface MPTopPullScroll (Private)
- (void)addAnimationTransform:(int)degree;

@end


@implementation MPTopPullScroll

@synthesize pullTitle       = _pullTitle;
@synthesize releaseTitle    = _releaseTitle;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {        
        _holding = NO;
        
        CGFloat y = kNavHeight-44;
        
        _navImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - 23)/2 - 100, y, 23, 44)];
        _navImageView.backgroundColor = [UIColor clearColor];
        _navImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:_navImageView];
        
        _title = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 200)/2, y, 200, 20)];
        _title.textAlignment = NSTextAlignmentCenter;
        _title.backgroundColor = [UIColor clearColor];
        _title.textColor = [UIColor whiteColor];//[UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        _title.text = @"";
        _title.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
        [self addSubview:_title];
        
        _subTitle = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 200)/2, y + 22, 200, 20)];
        _subTitle.textAlignment = NSTextAlignmentCenter;
        _subTitle.backgroundColor = [UIColor clearColor];
        _subTitle.textColor = [UIColor whiteColor]; //[UIColor colorWithRed:0.40 green:0.40 blue:0.40 alpha:1.0];
        _subTitle.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
        [self addSubview:_subTitle];
        
        UIImageView *sd = [[UIImageView alloc] initWithFrame:CGRectMake(0, kNavHeight, self.frame.size.width, 8)];
        sd.image = [UIImage imageNamed:@"shadow.png"];
        [self addSubview:sd];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


- (id)initHeaderWithPullTitle:(NSString *)pullTitle releaseTitle:(NSString *)releaseTitle {
    
    int width = 320;
    if([UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeLeft || [UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeRight)
    {
        width   =   [UIScreen mainScreen].bounds.size.height;
    }
    
    if (self = [self initWithFrame:CGRectMake(0, 0, width, kNavHeight)]) {
        self.pullTitle = pullTitle;
        self.releaseTitle = releaseTitle;
        _navImageView.image = [UIImage imageNamed:@"ArrowScroll.png"];
        [self addAnimationTransform:180];
    }
    return self;
}


- (void)setY:(CGFloat)y {
    CGRect rect = self.frame;
    
    if (y == rect.origin.y)  return;
    
    rect.origin.y = y;
    self.frame = rect;
    [self setNeedsDisplay]; 
}


- (void)layoutSubviews {
    
}


- (void)setTitle:(NSString *)title {
    _title.text = title;
}


- (void)holdScroll {
    if (_holding)  return;
    _subTitle.text = self.pullTitle;
    [self addAnimationTransform:180];
    _title.text = @"";
    _holding = YES;
}


- (void)releaseScroll {
    if (!_holding) return;
    _subTitle.text = self.releaseTitle;
    [self addAnimationTransform:180];
    _title.text = @"Loading...";
    _holding = NO;
}


- (void)resetArrow {
    [self addAnimationTransform:180];
}


- (void)stopAnimation {    
}


- (void)addAnimationTransform:(int)degree {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:self];
    
    CGAffineTransform transform = CGAffineTransformRotate(_navImageView.transform, MBDegreesToRadians(degree));
    _navImageView.transform = transform;
    
    [UIView commitAnimations];
}


//- (void)animationDidStop:(CAKeyframeAnimation *)anim finished:(BOOL)flag {
//    
//}


- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(context, true);
    
    CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.4);
    CGContextFillRect(context, CGRectMake(0, 0, self.frame.size.width, kNavHeight - 100));
    
    CGPoint start = CGPointMake(rect.size.width/2, kNavHeight - 100);
    CGPoint end = CGPointMake(rect.size.width/2, kNavHeight);
    
    /*CGFloat colors[] =
     {
     0.45, 0.45, 0.45, 0.6,
     0.35, 0.35, 0.35, 0.6,
     0.30, 0.30, 0.30, 0.6,
     };*/
    CGFloat colors[] =
    {
        0.0, 0.0, 0.0, 0.4,
        0.0, 0.0, 0.0, 0.5,
        0.0, 0.0, 0.0, 0.6
    };
    
    CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();        
    CGGradientRef gradient = CGGradientCreateWithColorComponents(rgb, colors,
                                                                 NULL, sizeof(colors)/(sizeof(colors[0])*4));
    CGColorSpaceRelease(rgb);    
    CGContextDrawLinearGradient(context, gradient, start, end, kCGGradientDrawsAfterEndLocation);                                    
    CGGradientRelease(gradient);
    
    CGContextSetLineWidth(context, 1.0);    
    CGContextSetRGBStrokeColor(context, 0.2, 0.2, 0.2, 0.7);
    
    CGContextMoveToPoint(context, 0, rect.size.height - 0.5);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height - 0.5);
    
    CGContextStrokePath(context);
    CGContextSetAllowsAntialiasing(context, false);
}



@end