//
//  ExtentionViewController.h
//  ReligiousLife
//
//  Created by Phonglenn on 4/21/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtentionViewController : UITableViewController
{
    NSMutableIndexSet *expandedSections;
    NSIndexPath* lastIndexPath;
    UISegmentedControl *segmentedControl;
}
@end
