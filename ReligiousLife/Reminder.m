//
//  Reminder.m
//  ReligiousLife
//
//  Created by harveynash on 3/25/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "Reminder.h"
#import "AppDelegate.h"

@implementation Reminder
@dynamic idRem;
@dynamic title;
@dynamic content;
@dynamic timeReminder;

+ (id)newReminder {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate.dataHelper insertEntityWithEntityName:@"ListReminder"];
}

+ (id)reminderFromDatabaseForId:(NSDate *)remId {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    Reminder *rem = nil;
    if (remId) {
        rem = [appDelegate.dataHelper getReminderForId:remId];
    }
    return rem;
}
+ (id)addNewReminder:(Reminder *)rem
{
    Reminder *newRem;
        
        /* try to get existing item */
        NSDate *idRem = rem.idRem;
        newRem = [Reminder reminderFromDatabaseForId:idRem];
        if(newRem)
        {
            if(newRem.timeReminder == rem.timeReminder)
            {
                return rem;
            }
        }
        else
        {
            rem = [Reminder newReminder];
        }
        newRem.idRem = rem.idRem;
        newRem.title = rem.title;
        newRem.content= rem.content;
        newRem.timeReminder = rem.timeReminder;

    return newRem;

}

@end
