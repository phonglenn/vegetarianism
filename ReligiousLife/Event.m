//
//  Event.m
//  ReligiousLife
//
//  Created by Phonglenn on 4/18/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "Event.h"

@implementation Event
@synthesize titleEventLabel;
@synthesize timeEventLabel;
@synthesize contentEventTextView;
#define LEFT_PADING_TITLE 5
#define TOP_PADING_TITLE 0


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-note"]];
        [self addSubview:backgroundView];
        // set title event
        self.titleEventLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_PADING_TITLE, TOP_PADING_TITLE, self.frame.size.width - LEFT_PADING_TITLE*2 - 50, 35)];
        self.titleEventLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:16];
        self.titleEventLabel.textColor = [UIColor grayColor];
        [self addSubview:self.titleEventLabel];
        
        // set time event
        self.timeEventLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, TOP_PADING_TITLE, 50, 35)];
        self.timeEventLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:14];
        self.timeEventLabel.textColor = [UIColor grayColor];
        [self addSubview:self.timeEventLabel];
        
        // set textview
        self.contentEventTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 35, self.frame.size.width, self.frame.size.height - 35)];
        self.contentEventTextView.textColor = [UIColor redColor];
        self.contentEventTextView.font = [UIFont fontWithName:@"UTM_Flamenco" size:14];
        [self.contentEventTextView setBackgroundColor:[UIColor clearColor]];
        self.contentEventTextView.editable = NO;
        [self addSubview:self.contentEventTextView];

    }
    return self;
}
-(void)setTitleAndContentOfEvent:(NSString *)title time:(NSString *)time content:(NSString *)content
{
    if(self.timeEventLabel)
    {
        self.titleEventLabel.text = title;
    }
    if(self.timeEventLabel)
    {
        self.timeEventLabel.text = time;
    }
    if(self.contentEventTextView)
    {
        self.contentEventTextView.text = content;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
