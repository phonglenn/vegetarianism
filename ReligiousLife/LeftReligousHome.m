//
//  LeftReligousHome.m
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "LeftReligousHome.h"
#import "ReferenceReligion.h"
#import "ReferenceVegetable.h"
#import "ReminderView.h"
#import "SWRevealViewController.h"

@implementation LeftReligousHome

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:14];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = NSLocalizedString(@"Menu",nil);
            cell.textLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:20];
            cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg-navi"]];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            break;
        case 1:
            cell.textLabel.text = NSLocalizedString(@"Tra cứu sách tôn giáo",nil);
            break;
            
        case 2:
            cell.textLabel.text = NSLocalizedString(@"Tra cứu các món ăn chay",nil);
            break;
        case 3:
            cell.textLabel.text = NSLocalizedString(@"Lịch nhắc nhở",nil);
            break;
        case 4:
            cell.textLabel.text = NSLocalizedString(@"Mở rộng",nil);
            break;
        case 5:
            cell.textLabel.text = NSLocalizedString(@"Trang nhà",nil);
            break;
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 1)
    {
        [self performSegueWithIdentifier:@"showReference" sender:nil];
    }
    else if (indexPath.row == 2)
    {
        [self performSegueWithIdentifier:@"showVegetable" sender:nil];
    }
    else if(indexPath.row == 3)
    {
        [self performSegueWithIdentifier:@"showReminder" sender:nil];
    }
    else if(indexPath.row == 4)
    {
        [self performSegueWithIdentifier:@"showExtention" sender:nil];
    }
    else
    {
        [self performSegueWithIdentifier:@"showHome" sender:nil];
    }
}
- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // Set the photo if it navigates to the PhotoView
//    if ([segue.identifier isEqualToString:@"showReference"]) {
//        ReferenceReligion *photoController = (ReferenceReligion*)segue.destinationViewController;
//    }
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
    
}

-(void)viewDidLoad
{
    UIImageView *imvBackground  =   [[UIImageView alloc] initWithFrame:self.tableView.frame];
    [imvBackground setImage:[UIImage imageNamed:@"background-wood"]];
    [self.tableView setBackgroundView:imvBackground];
    //make table view not show empty cell
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
@end
