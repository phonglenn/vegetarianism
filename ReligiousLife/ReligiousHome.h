//
//  ReligiousHome.h
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
@interface ReligiousHome : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
// date and month in calendar
@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *dayLabel;
// date and month in lunar
@property (strong, nonatomic) IBOutlet UILabel *dateColum;
@property (strong, nonatomic) IBOutlet UILabel *dateLunarLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeDateLunarLabel;

@property (strong, nonatomic) IBOutlet UILabel *monthColum;
@property (strong, nonatomic) IBOutlet UILabel *monthLunarLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeMonthLabel;
@property (nonatomic, strong) UIPopoverController *eventPopover;
@property (nonatomic, strong) Event *event;
@property (nonatomic, strong) NSArray *reminderList;
@end
