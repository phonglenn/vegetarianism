//
//  DetailReferVegetable.m
//  ReligiousLife
//
//  Created by harveynash on 3/13/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "DetailReferVegetable.h"

@implementation DetailReferVegetable
-(void)viewDidLoad
{
    self.title = @"Detail Vegetable";
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}
@end
