//
//  JSONHelper.m
//  Halalgems
//
//  Created by Nguyen Minh Khoai on 12/20/12.
//  Copyright (c) 2012 Nguyen The Phu. All rights reserved.
//

#import "JSONHelper.h"
#import "Reference.h"
#import "Vegetable.h"

@implementation JSONHelper

+ (BOOL)isSuccessfulRequest:(NSDictionary *)dict {

    NSString *message = [dict valueForKey:@"message"];
    NSInteger status = [[dict valueForKey:@"status"] integerValue];

    if(status == 200 || [[message lowercaseString] isEqualToString:@"successfully"])
        return YES;
    return NO;
}


+ (void)getListOfReferenceFromData:(NSDictionary *)dict
                    onCompleteBlock:(void (^)(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset))onCompeleteBlock {
    NSMutableArray *items = nil;
    NSInteger fileTotal = -1;
    NSInteger fileOffset = -1;

    if ([JSONHelper isSuccessfulRequest:dict]) {
        items = [NSMutableArray array];

        /* get reference list */
        NSArray *fileDicts = [dict valueForKey:@"data"];
        for (NSDictionary *fileDict in fileDicts) {
            [items addObject:[Reference referenceFromDictionary:fileDict]];
            fileOffset++;
        }

        
    }

    onCompeleteBlock(items, fileTotal, fileOffset);
}

+ (void)getListOfVegetableFromData:(NSDictionary *)dict
                   onCompleteBlock:(void (^)(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset))onCompeleteBlock {
    NSMutableArray *items = nil;
    NSInteger fileTotal = -1;
    NSInteger fileOffset = -1;
    
    if ([JSONHelper isSuccessfulRequest:dict]) {
        items = [NSMutableArray array];
        
        /* get reference list */
        NSArray *fileDicts = [dict valueForKey:@"data"];
        for (NSDictionary *fileDict in fileDicts) {
            [items addObject:[Vegetable vegetableFromDictionary:fileDict]];
            fileOffset++;
        }
        
        
    }
    
    onCompeleteBlock(items, fileTotal, fileOffset);
}



@end

