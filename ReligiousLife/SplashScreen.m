//
//  SplashScreen.m
//  ReligiousLife
//
//  Created by harveynash on 4/18/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "SplashScreen.h"
#import "SliderScreen.h"

@interface SplashScreen ()

@end

@implementation SplashScreen
@synthesize splashImage;
@synthesize randomImage;
@synthesize quotaLabel;
@synthesize authorLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    // fill content quota
    self.quotaLabel.numberOfLines = 0;
    self.quotaLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.quotaLabel.textColor = [UIColor whiteColor];
    self.quotaLabel.font = [UIFont fontWithName:@"UTM_Netmuc_KT" size:20];
    self.quotaLabel.text = @"Không gì ích lợi cho sức khỏe của con người để có cơ hội sống lâu trên quả địa cầu này bằng cách ăn chay.";
    
    // fill content author
    self.authorLabel.numberOfLines = 0;
    self.authorLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.authorLabel.textColor = [UIColor whiteColor];
    self.authorLabel.font = [UIFont fontWithName:@"UTM_Netmuc_KT" size:22];
    self.authorLabel.text = @"Albert Einstein.";
    
    self.splashImage.image = [UIImage imageNamed:@"background-wood"];
    self.randomImage.image = [UIImage imageNamed:@"background-image-splash-screen"];
    
    
     [NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(dismissSplashScreen) userInfo:nil repeats:NO];

}
-(void)dismissSplashScreen
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        // app already launched
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
        // This is the first launch ever
        SliderScreen *sliderScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"SliderScreen"];
        [self.navigationController pushViewController:sliderScreen animated:NO];
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
