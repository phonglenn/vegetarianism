//
//  AddEventViewController.m
//  ReligiousLife
//
//  Created by harveynash on 3/12/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "AddEventViewController.h"

@implementation AddEventViewController
-(void)viewDidLoad
{
    self.title = @"Add Event";
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}
@end
