//
//  ReferenceVegetable.m
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "ReferenceVegetable.h"
#import "SWRevealViewController.h"
#import "DetailRefReligion.h"
#import "AppDelegate.h"
#import "itemVegetable.h"

#define limitFetchVeg 3
#define limitFetchRef 3
#define ITEM_ORIGIN_X 35
#define ITEM_WIDTH 110
#define ITEM_HEIGHT 100
#define ITEM_PADING 5
#define SEPARATOR_HEIGHT 7
@implementation ReferenceVegetable
{
    BOOL loading;
}
@synthesize vegetableRefArray;
@synthesize searchResultsArray;
@synthesize pageIndex;
#pragma Loading Table
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return ([self.searchResultsArray count] +1 )/2;
        
    } else {
        return ([self.vegetableRefArray count] +1 )/2;
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellVegetable";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    Vegetable *veg1 = nil,*veg2= nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        veg1 = [searchResultsArray objectAtIndex:indexPath.row*2];
        if([searchResultsArray count] % 2 == 0)
        {
            veg2 = [searchResultsArray objectAtIndex:indexPath.row*2+1];
        }
        
    } else {
        veg1 = [vegetableRefArray objectAtIndex:indexPath.row*2];
        if(indexPath.row*2 + 1 < [vegetableRefArray count])
        {
            veg2 = [vegetableRefArray objectAtIndex:indexPath.row*2+1];
        }
        
    }
    
    if(veg1 != nil)
    {
        itemVegetable *newItem1 = [[itemVegetable alloc] initWithFrame:CGRectMake(ITEM_ORIGIN_X, ITEM_PADING, ITEM_WIDTH, ITEM_HEIGHT)];
        [newItem1 setTitleAndImage:veg1.title image:nil tag:[veg1.idVeg integerValue]];
        [newItem1.customButton addTarget:self action:@selector(itemReferenceClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:newItem1];
    }
    if(veg2 != nil)
    {
        itemVegetable *newItem2 = [[itemVegetable alloc] initWithFrame:CGRectMake(cell.frame.size.width - ITEM_ORIGIN_X - ITEM_WIDTH, ITEM_PADING, ITEM_WIDTH, ITEM_HEIGHT)];
        [newItem2 setTitleAndImage:veg2.title image:nil tag:[veg2.idVeg integerValue]];
        [newItem2.customButton addTarget:self action:@selector(itemReferenceClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:newItem2];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *imagView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell-seperate-big"]];
    CGRect frame = cell.frame;
    frame.size.height = 110;
    cell.frame = frame;
    imagView.frame = CGRectMake(cell.textLabel.frame.origin.x + 5,cell.frame.size.height - 3.0, cell.frame.size.width - (cell.textLabel.frame.origin.x + 5)*2, SEPARATOR_HEIGHT);
    [cell.contentView addSubview:imagView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)viewDidLoad
{
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _pagingScroll = YES;
    // pull to refresh
    _topPullArrow = [[MPTopPullScroll alloc] initHeaderWithPullTitle:PULL_TO_REFRESH
                                                        releaseTitle:RELEASE_TO_REFRESH];
    [_topPullArrow setY:-(_topPullArrow.frame.size.height)];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    // update reference from server
    [self requestVegetableFromServer];
}
-(void)viewDidDisappear:(BOOL)animated
{
    pageIndex = 0;
    self.vegetableRefArray = nil;
    self.searchResultsArray = nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    pageIndex = 0;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.vegetableRefArray = [NSMutableArray arrayWithArray:[appDelegate.dataHelper getObjectInTable:@"Vegetable" fromOffset:(pageIndex * limitFetchVeg) limit:limitFetchVeg]];

    // set title and navigation
    self.title = NSLocalizedString(@"Thư viện nấu ăn",nil);
    
    UIImage *menuImage=nil;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
    {
        menuImage = [UIImage imageNamed:@"menu-icon"];
    }
    else
    {
        menuImage = [[UIImage imageNamed:@"menu-icon"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:menuImage style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background-wood"]]];
}

#pragma Filter - Display Search Bar
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    // search from database
    searchResultsArray = [vegetableRefArray filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
#pragma mark - Item Reference Click
-(void)itemReferenceClick:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if(button.tag)
    {
        //
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                       initWithTitle:NSLocalizedString(@"Back", nil)
                                       style:UIBarButtonItemStylePlain
                                       target:nil
                                       action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        
        DetailRefReligion *detailRef = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailRefReligion"];
        [self.navigationController pushViewController:detailRef animated:YES];

    }
}
#pragma mark - Get from database
- (void) loadMoreVegetable
{
    pageIndex++;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *loadMoreRef = [NSArray arrayWithArray:[appDelegate.dataHelper getObjectInTable:@"Vegetable" fromOffset:(pageIndex * limitFetchVeg) limit:limitFetchVeg]];
    for(Vegetable *veg in loadMoreRef)
    {
        // need to sort result
        [self.vegetableRefArray addObject:veg];
    }
    [self.tableView reloadData];
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:([self.listRef count]-1) inSection:0] atScrollPosition:(UITableViewScrollPositionTop) animated:NO];
    
}

#pragma mark - REQUEST VEGETABLE API
-(void)requestVegetableFromServer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.hasNetwork)
    {
        [self showLoadingView];
        [appDelegate.dataCenter getListOfAllVegetableWithOffset:appDelegate.pageIndexVeg limit:4 successBlock:^(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset)
         {
             [self hideLoadingView];
             if([items count] >0)
             {
                 appDelegate.pageIndexVeg++;
                 pageIndex = 0;
                 [appDelegate.dataHelper saveContext];
                 // need to sort result
                 self.vegetableRefArray = [NSMutableArray arrayWithArray:[appDelegate.dataHelper getObjectInTable:@"Vegetable" fromOffset:(pageIndex * limitFetchVeg) limit:limitFetchVeg]];
                 [self.tableView reloadData];
             }
             
         }errorBlock:^(id error) {
             [self hideLoadingView];
             NSLog(@"request failed");
         }];
    }
}

#pragma mark - HANDLE REFRESH DATA
- (void)handleReleaseAction
{
    [self requestVegetableFromServer];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // UITableView only moves in one direction, y axis
    if (!_pagingScroll) return;
    _resetOffset = NO;
    
    if (_released) {
        [self handleReleaseAction];
        return;
    }
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
         [self loadMoreVegetable];
        //[self methodThatAddsDataAndReloadsTableView];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_pagingScroll)
        return;
    _offset = scrollView.contentOffset.y;
    _released = FALSE;
    
    if (_offset < -64)
    {
        if (![_topPullArrow superview]) {
            [scrollView addSubview:_topPullArrow];
        }
        
        if (abs(_offset) < kScrollLimit) {
            [_topPullArrow holdScroll];
        } else {
            [_topPullArrow releaseScroll];
            _released = TRUE;
        }
    } else {
        if (_topPullArrow && [_topPullArrow superview]) {
            [_topPullArrow removeFromSuperview];
        }
    }
}
#pragma mark - MBProgressHUB

- (void)showLoadingViewWithMessage:(NSString *)message {
    if (!_progressView) {
        
        CGFloat x = (self.view.bounds.size.width - kLoadingViewWidth) / 2;
        CGFloat y = (self.view.bounds.size.height - kLoadingViewHeight) / 2;
        CGRect rect = CGRectMake(x, y, kLoadingViewWidth, kLoadingViewHeight);
        
        
        self.progressView = [[ProgressView alloc] initWithFrame:rect];
        [_progressView setTextColor:[UIColor whiteColor]];
        [_progressView setActivityColor:UIColorFromRGB(0x41AEC8)];
        _progressView.activityView.center = CGPointMake(_progressView.frame.size.width / 2, _progressView.frame.size.height / 2);
        _progressView.backgroundColor = [UIColor clearColor];
        [_progressView setBackgroundImage:nil];
    }
    
    [_progressView setTitle:message];
    [_progressView bringSubviewToFront:self.view];
    [_progressView showOnView:self.view animation:YES freeze:YES top:IS_IPAD ? self.navigationController.navigationBar.frame.size.height : 0];
}

- (void)showLoadingView {
    [self showLoadingViewWithMessage:@"Updating..."];
    loading = YES;
}

- (void)hideLoadingView {
    [_progressView performSelector:@selector(hide) onThread:[NSThread mainThread] withObject:nil waitUntilDone:YES];
    loading = NO;
}
@end
