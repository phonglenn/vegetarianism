/*============================================================================
 PROJECT: Common
 FILE:    BaseNetwork.h
 AUTHOR:  Nguyen Minh Khoai
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Json.h"
#import "DataCenter.h"

/*============================================================================
 MACRO
 =============================================================================*/
//#define TEST_SERVICE
#ifdef  TEST_SERVICE
#define kBaseService                @"https://api-test.kleii.com"
#else
#define kBaseService                @"https://api.viettel.kleii.info"
#endif

#define kNoConnectionErrorMessage       NSLocalizedString(@"No connection. Please check your network", @"Error Message")
#define kServiceNotAvailableMessage     NSLocalizedString(@"The network service is currently unavailable. Please try again later.", @"Error Message")

/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/
/*----------------------------------------------------------------------------
 Interface:   BaseNetwork
 -----------------------------------------------------------------------------*/

@interface BaseNetwork : NSObject

/* properties */
@property(nonatomic, strong) AFHTTPClient *networkEngine;
@property(nonatomic, assign) BOOL enableDebugLog;

- (id)initWithBaseURL:(NSString *)baseUrlString;

+ (BaseNetwork *)service;

/* for normal request */
- (void)requestWithURL:(NSString *)urlString
                params:(NSMutableDictionary *)params
          successBlock:(void (^)(NSDictionary *))successBlock
            errorBlock:(void (^)(NSString *))errorBlock
            httpMethod:(NSString *)method;

/* for SOAP request */
- (void)requestWithHttpBody:(NSString *)httpBody
               headerFields:(NSMutableDictionary *)fields
               successBlock:(void (^)(id))successBlock
                 errorBlock:(void (^)(NSString *))errorBlock
                 httpMethod:(NSString *)method;

/* for form request */
- (void)formRequestWithURLPath:(NSString *)urlString
                        params:(NSMutableDictionary *)params
                  successBlock:(void (^)(NSDictionary *))successBlock
                    errorBlock:(void (^)(NSString *))errorBlock;

- (void)formRequestWithURLPath:(NSString *)urlString
                        params:(NSMutableDictionary *)params
                      filePath:(NSString *)filePath
                          name:(NSString *)name
                  successBlock:(void (^)(NSDictionary *))successBlock
                    errorBlock:(void (^)(NSString *))errorBlock;

- (AFHTTPRequestOperation *)formRequestWithURLPath:(NSString *)urlString
                                            params:(NSMutableDictionary *)params
                                             image:(UIImage *)image
                                      successBlock:(void (^)(NSDictionary *))successBlock
                                        errorBlock:(void (^)(NSString *))errorBlock
                                     progressBlock:(void (^)(long long totalBytesWritten, long long totalBytesExpectedToWrite))progressBlock;

@end
