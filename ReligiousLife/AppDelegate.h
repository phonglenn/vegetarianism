//
//  AppDelegate.h
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "CCCoreDataHelper.h"
#import "DataCenter.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,CoreDataDelegate>
{
    Reachability *networkMonitor;
    BOOL hasNetwork;
}
@property(strong, nonatomic) Reachability *networkMonitor;
@property(nonatomic, assign) BOOL hasNetwork;

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(strong, nonatomic) DataCenter *dataCenter;
@property(nonatomic, strong) CCCoreDataHelper *dataHelper;

@property(nonatomic, assign) int pageIndexRef;
@property(nonatomic, assign) int pageIndexVeg;
//- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
