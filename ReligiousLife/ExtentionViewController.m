//
//  ExtentionViewController.m
//  ReligiousLife
//
//  Created by Phonglenn on 4/21/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "ExtentionViewController.h"
#import "SWRevealViewController.h"
@interface ExtentionViewController ()

@end

@implementation ExtentionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *itemSegment = [NSArray arrayWithObjects: @"Đóng", @"Mở", nil];
    segmentedControl = [[UISegmentedControl alloc] initWithItems:itemSegment];
    segmentedControl.selectedSegmentIndex = 1;
    segmentedControl.backgroundColor = [UIColor brownColor];
    segmentedControl.tintColor = [UIColor blackColor];
    [segmentedControl addTarget:self action:@selector(pickOne:) forControlEvents:UIControlEventValueChanged];
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    // set title and navigation bar
    self.title = NSLocalizedString(@"Mở rộng",nil);
    
    UIImage *menuImage=nil;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
    {
        menuImage = [UIImage imageNamed:@"menu-icon"];
    }
    else
    {
        menuImage = [[UIImage imageNamed:@"menu-icon"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:menuImage style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background-wood"]]];
    UIImageView *imvBackground  =   [[UIImageView alloc] initWithFrame:self.tableView.frame];
    [imvBackground setImage:[UIImage imageNamed:@"background-wood"]];
    //[self.tableView setBackgroundView:imvBackground];
    //make table view not show empty cell
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([expandedSections containsIndex:section])
        {
            return 5; // return rows when expanded
        }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 1;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section == 1)
    {
        return YES;
    }
    
    return NO;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:14];
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        UIImageView *imagView;
        
        if (!indexPath.row)
        {
            cell.textLabel.text = @"Chọn ngày";
            //if ([expandedSections containsIndex:indexPath.section])
            {
                UIButton *expandButton = [[UIButton alloc] initWithFrame:CGRectMake(224, (cell.frame.size.height- 25)/2, 80, 25)];
                [expandButton setTitle:@"5 ngày" forState:UIControlStateNormal];
                expandButton.backgroundColor = [UIColor brownColor];
                expandButton.layer.cornerRadius = 4.0f;
                expandButton.titleLabel.textColor = [UIColor grayColor];
                expandButton.titleLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:14];
                [expandButton addTarget:self action:@selector(expandButtonClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:expandButton];
                
                imagView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell-seperate"]];
                imagView.frame = CGRectMake(cell.textLabel.frame.origin.x,cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width - cell.textLabel.frame.origin.x*2, 1);
            }
        }
        else
        {
            imagView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"subcell-seperate"]];
            imagView.frame = CGRectMake(cell.textLabel.frame.origin.x - 5,cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width - (cell.textLabel.frame.origin.x - 5)*2, 1);
            if(lastIndexPath.row == indexPath.row)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            switch (indexPath.row) {
                case 1:
                    cell.textLabel.text = NSLocalizedString(@"2 ngày/tháng: mùng 01/15", nil);
                    break;
                case 2:
                    cell.textLabel.text = NSLocalizedString(@"3 ngày/tháng: mùng 01/15/19", nil);
                    break;
                case 3:
                    cell.textLabel.text = NSLocalizedString(@"4 ngày/tháng: mùng 14/15/30/01", nil);
                    break;
                case 4:
                    cell.textLabel.text = NSLocalizedString(@"5 ngày/tháng: mùng 14/15/19/30/01", nil);
                    break;
                default:
                    break;
            }
        }
        [cell.contentView addSubview:imagView];
    }
    else
    {
    
    switch (indexPath.section) {
        case 0:
            segmentedControl.frame = CGRectMake(250, (cell.frame.size.height- 25)/2, 80, 25);
            cell.textLabel.text = NSLocalizedString(@"Nhắc nhở",nil);
            cell.accessoryView = segmentedControl;
            break;
        case 1:
            cell.textLabel.text = NSLocalizedString(@"Chọn ngày",nil);
            break;
        case 2:
            cell.textLabel.text = NSLocalizedString(@"Lịch Sự Kiện",nil);
            break;
        case 3:
            cell.textLabel.text = NSLocalizedString(@"Mở rộng",nil);
            break;
        case 4:
            cell.textLabel.text = NSLocalizedString(@"Trang nhà",nil);
            break;
        default:
            break;
    }
    }
   
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
                
            }
            else
            {
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            
            
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                
                //cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
                
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                //cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
                
            }
        }
        else
        {
            int newRow = [indexPath row];
            int oldRow = (lastIndexPath != nil) ? [lastIndexPath row] : -1;
            
            if(newRow != oldRow)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                UITableViewCell* oldCell = [tableView cellForRowAtIndexPath:lastIndexPath];
                oldCell.accessoryType = UITableViewCellAccessoryNone;
                lastIndexPath = indexPath;
            }
        }
    }
}

#pragma mark - Layout TableView
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]init];
    [view setAlpha:0.0F];
    return view;
    
}

#pragma mark - Button Click
-(void)expandButtonClick:(id)sender
{
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
}
-(void)pickOne:(id)sender
{
}
@end
