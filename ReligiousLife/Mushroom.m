//
//  Mushroom.m
//  ReligiousLife
//
//  Created by Phonglenn on 4/16/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "Mushroom.h"

@implementation Mushroom

#define widthMushroom 13
#define highMushroom 10
@synthesize activeImage;
@synthesize inactiveImage;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.numberOfPages = 4;
        self.currentPage = 0;
        activeImage = [UIImage imageNamed:@"pagecontrol-active"];
        inactiveImage = [UIImage imageNamed:@"pagecontrol-inactive"];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
/** override to update dots */
- (void) setCurrentPage:(NSInteger)currentPage
{
    [super setCurrentPage:currentPage];
    
    // update dot views
    [self updateDots];
}

/** override to update dots */
- (void) setNumberOfPages:(NSInteger)number
{
    [super setNumberOfPages:number];
    
    // update dot views
    [self updateDots];
}

/** override to update dots */
- (void) updateCurrentPageDisplay
{
    [super updateCurrentPageDisplay];
    
    // update dot views
    [self updateDots];
}

/** Override setImageNormal */
- (void) setImageNormal:(UIImage*)image
{
    inactiveImage = nil;
    inactiveImage = [UIImage imageWithCGImage:image.CGImage];
    
    // update dot views
    [self updateDots];
}

/** Override setImageCurrent */
- (void) setImageCurrent:(UIImage*)image
{
    activeImage = nil;
    activeImage = [UIImage imageWithCGImage:image.CGImage];
    
    // update dot views
    [self updateDots];
}

/** Override to fix when dots are directly clicked */
- (void) endTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
    [super endTrackingWithTouch:touch withEvent:event];
    
    [self updateDots];
}
-(void) updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIImageView * dot = [self imageViewForSubview:  [self.subviews objectAtIndex: i]];
        if (i == self.currentPage) dot.image = activeImage;
        else dot.image = inactiveImage;

    }
     
}

- (UIImageView *) imageViewForSubview: (UIView *) view
{
    UIImageView * dot = nil;
    if ([view isKindOfClass: [UIView class]])
    {
        view.backgroundColor = [UIColor clearColor];
        for (UIView* subview in view.subviews)
        {
            if ([subview isKindOfClass:[UIImageView class]])
            {
                dot = (UIImageView *)subview;
                break;
            }
        }
        if (dot == nil)
        {
            dot = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, widthMushroom, highMushroom)];
            NSLog(@"width is %f, height is %f.", view.frame.size.width, view.frame.size.height);
            [view addSubview:dot];
        }

    }
    else
    {
        dot = (UIImageView *) view;
    }
    
    return dot;
}


@end
