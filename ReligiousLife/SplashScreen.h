//
//  SplashScreen.h
//  ReligiousLife
//
//  Created by harveynash on 4/18/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashScreen : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *splashImage;
@property (strong, nonatomic) IBOutlet UILabel *quotaLabel;
@property (strong, nonatomic) IBOutlet UILabel *authorLabel;
@property (strong, nonatomic) IBOutlet UIImageView *randomImage;

@end
