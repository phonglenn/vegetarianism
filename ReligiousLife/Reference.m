//
//  Item.m
//  Kleii
//
//  Created by Khoai Nguyen on 7/10/13.
//
//

#import "Reference.h"
#import "AppDelegate.h"

@implementation Reference

@dynamic idRef;
@dynamic title;
@dynamic content;
@dynamic category;
@dynamic lastUpdate;

+ (id)newRefernce {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate.dataHelper insertEntityWithEntityName:@"Reference"];
}

+ (id)referenceFromDatabaseForId:(NSString *)refId {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    Reference *ref = nil;
    if (refId.length > 0) {
        ref = [appDelegate.dataHelper getReferenceForId:refId];
    }
    return ref;
}
+ (id)referenceFromDictionary:(NSDictionary *)dict {
    Reference *ref;
    if (dict.allKeys.count > 0) {
        
            /* try to get existing item */
            NSString *refId = [dict valueForKey:@"id"];
            ref = [Reference referenceFromDatabaseForId:refId];
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            NSDate* serverDate = [dateFormatter dateFromString:[dict valueForKey:@"date_created"]];
            if(ref)
            {
                if(ref.lastUpdate == serverDate)
                {
                    return ref;
                }
            }
            else
            {
                ref = [Reference newRefernce];
            }
            ref.idRef = [dict valueForKey:@"id"];
            ref.title = [dict valueForKey:@"title"];
            ref.content = [dict valueForKey:@"content"];
            ref.lastUpdate = serverDate;
        }
    return ref;
}

@end
