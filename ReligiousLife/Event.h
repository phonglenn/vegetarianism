//
//  Event.h
//  ReligiousLife
//
//  Created by Phonglenn on 4/18/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EventDelegate <NSObject>

-(void)didSelectEvent;

@end

@interface Event : UIView<EventDelegate>
@property (strong,nonatomic) UILabel *titleEventLabel;
@property (strong,nonatomic) UILabel *timeEventLabel;
@property (strong,nonatomic) UITextView *contentEventTextView;
@property (strong,nonatomic) id<EventDelegate> delegate;
-(void)setTitleAndContentOfEvent:(NSString *)title time:(NSString *)time content:(NSString *)content;
@end
