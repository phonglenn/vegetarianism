//
//  Mushroom.h
//  ReligiousLife
//
//  Created by Phonglenn on 4/16/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Mushroom : UIPageControl
{
    UIImage* activeImage;
    UIImage* inactiveImage;
}
@property (nonatomic, readwrite, retain) UIImage* inactiveImage;
@property (nonatomic, readwrite, retain) UIImage* activeImage;
@end
