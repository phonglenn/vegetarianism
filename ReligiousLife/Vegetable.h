//
//  Vegetable.h
//  ReligiousLife
//
//  Created by harveynash on 3/25/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Vegetable : NSManagedObject
@property(nonatomic, strong) NSString *idVeg;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *content;
@property(nonatomic, strong) NSDate *lastUpdate;


+ (id)newVegetable;

+ (id)vegetableFromDatabaseForId:(NSString *)vegId;

+ (id)vegetableFromDictionary:(NSDictionary *)dict;

@end
