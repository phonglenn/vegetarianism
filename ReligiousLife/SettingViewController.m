//
//  SettingViewController.m
//  ReligiousLife
//
//  Created by harveynash on 3/12/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "SettingViewController.h"

@implementation SettingViewController
-(void)viewDidLoad
{
    self.title = @"Setting";
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}
@end
