//
//  SplashScreen.m
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "SliderScreen.h"

@implementation SliderScreen
@synthesize welcomeScroll;
@synthesize imageArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidLoad
{
    pageIndicator = [[Mushroom alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-44, self.view.frame.size.width, 30)];
    [pageIndicator addTarget:self action:@selector(pageChanged) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:pageIndicator];
    [self.view bringSubviewToFront:pageIndicator];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    //Put the names of our image files in our array.
    imageArray = [[NSArray alloc] initWithObjects:@"slide1", @"slide2", @"slide3",@"slide4", nil];
    
    for (int i = 0; i < [imageArray count]; i++) {
        //Create an imageView object in every 'page' of our scrollView.
        CGRect frame;
        frame.origin.x = self.welcomeScroll.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.welcomeScroll.frame.size;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = [UIImage imageNamed:[imageArray objectAtIndex:i]];
        [self.welcomeScroll addSubview:imageView];
    }
    //Set the content size of our scrollview according to the total width of our imageView objects.
    welcomeScroll.contentSize = CGSizeMake(welcomeScroll.frame.size.width * [imageArray count], welcomeScroll.frame.size.height);
                                        
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
}
                                                

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.welcomeScroll.frame.size.width;
    int page = floor((self.welcomeScroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self->pageIndicator.currentPage = page;
}
-(void) pageChanged
{
    int page = pageIndicator.currentPage;
    
    // update the scroll view to the appropriate page
    CGRect frame = welcomeScroll.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [welcomeScroll scrollRectToVisible:frame animated:YES];
}
@end
