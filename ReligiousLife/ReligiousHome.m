//
//  ReligiousHome.m
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "ReligiousHome.h"
#import "SplashScreen.h"
#import "SWRevealViewController.h"
#import "SettingViewController.h"
#import "Reminder.h"

@implementation ReligiousHome
@synthesize backgroundImage;
@synthesize monthLabel, dateLabel, dayLabel;
@synthesize dateColum, dateLunarLabel, timeDateLunarLabel;
@synthesize monthColum, monthLunarLabel, timeMonthLabel;
@synthesize eventPopover,event,reminderList;
#define MAX_EVENT 3
#define ARROW_POSITION 50

bool isDismisSplashSceen = NO;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidLoad
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg-navi"] forBarMetrics:UIBarMetricsDefault];
    self.backgroundImage.image = [UIImage imageNamed:@"background-wood"];
    
    NSDictionary *settings = @{
                               UITextAttributeFont                 :  [UIFont fontWithName:@"UTM_Netmuc_KT" size:20],
                               UITextAttributeTextColor            :  [UIColor whiteColor],
                               UITextAttributeTextShadowOffset     :  [NSValue valueWithUIOffset:UIOffsetZero]};
    
    [[UINavigationBar appearance] setTitleTextAttributes:settings];
    
    self.title = NSLocalizedString(@"Sự kiện trong ngày", nil);
    
    // set date and time in calendar
    self.monthLabel.text = @"Tháng 4/2014";
    self.monthLabel.textColor = [UIColor grayColor];
    self.monthLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:18];
    
    self.dateLabel.text = @"5";
    self.dateLabel.textColor = [UIColor whiteColor];
    self.dateLabel.numberOfLines = 0;
    self.dateLabel.lineBreakMode = NSLineBreakByWordWrapping;
    if (IS_568_SCREEN)
    {
        self.dateLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:150];
    }
    else
    {
        self.dateLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:80];
    }
    
    NSString *day = @"chủ nhật";
    self.dayLabel.text = [day uppercaseString];
    self.dayLabel.textColor = [UIColor grayColor];
    self.dayLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:18];
    
    // set date in lunar
    self.dateColum.text = NSLocalizedString(@"Ngày", nil);
    self.dateColum.textColor = [UIColor whiteColor];
    self.dateColum.font = [UIFont fontWithName:@"UTM_Flamenco" size:16];
    
    self.dateLunarLabel.text = @"31";
    self.dateLunarLabel.textColor = [UIColor grayColor];
    self.dateLunarLabel.numberOfLines = 0;
    self.dateLunarLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.dateLunarLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:66];
    
    self.timeDateLunarLabel.text = @"Đinh mùi";
    self.timeDateLunarLabel.textColor = [UIColor whiteColor];
    self.timeDateLunarLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:16];
    
    // set date in lunar
    self.monthColum.text = NSLocalizedString(@"Tháng", nil);
    self.monthColum.textColor = [UIColor whiteColor];
    self.monthColum.font = [UIFont fontWithName:@"UTM_Flamenco" size:16];
    
    self.monthLunarLabel.text = @"5";
    self.monthLunarLabel.textColor = [UIColor grayColor];
    self.monthLunarLabel.numberOfLines = 0;
    self.monthLunarLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.monthLunarLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:85];
    
    self.timeMonthLabel.text = @"Mậu thìn";
    self.timeMonthLabel.textColor = [UIColor whiteColor];
    self.timeMonthLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:16];
    
    // create circle image
    [self getEventList];
    
    
}
#pragma Get Event List
-(void)getEventList
{
    for(int i=0;i < 3; i++)
    {
        UIImageView* imgView = [[UIImageView alloc] initWithFrame:CGRectMake(80 + i*70 , 270, 40, 40)];
        imgView.image = [UIImage imageNamed:@"homescreen"];
        imgView.layer.cornerRadius = imgView.frame.size.height/2;
        imgView.layer.borderWidth = 2;
        [imgView.layer setBorderColor: [[UIColor blackColor] CGColor]];
        imgView.layer.masksToBounds = YES;
        [self.view addSubview:imgView];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = imgView.frame;
        button.tag = i + 1;
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.layer.cornerRadius = imgView.frame.size.height/2;
        [self.view addSubview:button];
    }
}
-(void)buttonClick:(id)sender
{
    NSLog(@"button click");
    
    UIButton *button = (UIButton *)sender;

    [event removeFromSuperview];
    self.event = [[Event alloc] initWithFrame:CGRectMake(button.frame.origin.x - ARROW_POSITION, button.frame.origin.y + button.frame.size.height, 220, 96)];
    
    NSString *title1 = @"ĐI PHÓNG SANH";
    NSString *title2 = @"ĐI TỪ THIỆN";
    NSString *content = @"Mua cá - Mua bông trái, nhan đèn chuẩn bị cho việc phóng sanh.";
    if(button.tag -1 == 0)
    {
        [event setTitleAndContentOfEvent:title1 time:@"8:00 am" content:content];
    }
    else
    {
        [event setTitleAndContentOfEvent:title2 time:@"8:00 am" content:content];
    }
    [self.view addSubview:event];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    if(!isDismisSplashSceen)
    {
        isDismisSplashSceen = YES;
        SplashScreen *splashScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"SplashScreen"];
        [self.navigationController pushViewController:splashScreen animated:NO];
    }
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    UIImage *menuImage=nil;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
    {
        menuImage = [UIImage imageNamed:@"menu-icon"];
    }
    else
    {
        menuImage = [[UIImage imageNamed:@"menu-icon"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:menuImage style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    
    //[self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

}

#pragma Button Items Click

-(void)settingButtonClick
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:NSLocalizedString(@"Back", nil)
                                   style:UIBarButtonItemStylePlain
                                   target:nil
                                   action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    SettingViewController *setting = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [self.navigationController pushViewController:setting animated:YES];

}

@end
