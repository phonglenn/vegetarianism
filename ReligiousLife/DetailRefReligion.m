//
//  DetailRefReligion.m
//  ReligiousLife
//
//  Created by harveynash on 3/12/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "DetailRefReligion.h"

@implementation DetailRefReligion
@synthesize itemImage;
@synthesize materialLabel, materialTextView;
@synthesize processLabel, processScrollView, processTextView;
@synthesize imageArray;
-(void)viewDidLoad
{
    imageArray = [[NSArray alloc] initWithObjects:@"homescreen", @"homescreen", @"homescreen",@"homescreen", nil];
    for (int i = 0; i < [imageArray count]; i++) {
        //Create an imageView object in every 'page' of our scrollView.
        CGRect frame;
        frame.origin.x = self.processScrollView.frame.size.width/2 * i;
        frame.origin.y = 0;
        frame.size = self.processScrollView.frame.size;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = [UIImage imageNamed:[imageArray objectAtIndex:i]];
        [self.processScrollView addSubview:imageView];
    }
    //Set the content size of our scrollview according to the total width of our imageView objects.
    processScrollView.contentSize = CGSizeMake(processScrollView.frame.size.width/2 * [imageArray count], processScrollView.frame.size.height);
    // fill data
    self.title = @"Detail Reference";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-wood"]];
    self.itemImage.image = [UIImage imageNamed:@"homescreen"];
    self.itemImage.layer.cornerRadius = 7.0f;
    self.itemImage.layer.masksToBounds = YES;
    self.itemImage.layer.borderWidth = 3;
    [self.itemImage.layer setBorderColor:[[UIColor whiteColor] CGColor]];
    
    self.materialLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:16];
    self.materialLabel.textColor = [UIColor whiteColor];
    self.materialLabel.text = NSLocalizedString(@"Nguyên liệu", nil);
    
    self.materialTextView.textColor = [UIColor whiteColor];
    self.materialTextView.font = [UIFont fontWithName:@"UTM_Flamenco" size:14];
    [self.materialTextView setBackgroundColor:[UIColor clearColor]];
    self.materialTextView.editable = NO;
    self.materialTextView.text = @"- trứng 4 quả - sữa tươi ko đường :35gr - dầu ăn :35gr - bột mỳ :50gr - bột ngô :50gr - chanh :1/4 quả (lấy nước cốt)";
    
    self.processLabel.font = [UIFont fontWithName:@"UTM_Flamenco" size:17];
    self.processLabel.textColor = [UIColor whiteColor];
    self.processLabel.text = NSLocalizedString(@"Thực hiện", nil);
    
    self.processTextView.textColor = [UIColor whiteColor];
    self.processTextView.font = [UIFont fontWithName:@"UTM_Flamenco" size:13];
    [self.processTextView setBackgroundColor:[UIColor clearColor]];
    self.processTextView.editable = NO;
    
    self.processTextView.text = @" - Trứng tách lòng trắng, lòng đỏ riêng - Cho dầu ăn, sữa tươi vào lòng đỏ, dùng phới đánh bằng tay cho nhuyễn - Cho bột mỳ, bột ngô vào đánh nhuyễn cùng";
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
}
#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.processScrollView.frame.size.width/2;
    int page = floor((self.processScrollView.contentOffset.x - pageWidth / 4) / pageWidth) + 1;

    CGRect frame = processScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [processScrollView scrollRectToVisible:frame animated:YES];
}

@end
