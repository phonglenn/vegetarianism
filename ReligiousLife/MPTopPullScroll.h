//
//  MPTopPullScroll.h
//  MobionPhoto
//
//  Created by Han Korea on 11/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPTopPullScroll : UIView {
    UILabel        *_title;
    UILabel        *_subTitle;
    UIImageView    *_navImageView;
    BOOL            _holding; 
    NSString       *_pullTitle;
    NSString       *_releaseTitle;
}

@property(nonatomic, retain) NSString *pullTitle;
@property(nonatomic, retain) NSString *releaseTitle;

- (id)initHeaderWithPullTitle:(NSString *)pullTitle releaseTitle:(NSString *)releaseTitle;

- (void)holdScroll;
- (void)releaseScroll;
- (void)setTitle:(NSString *)title;
- (void)setY:(CGFloat)y;
- (void)resetArrow;
@end