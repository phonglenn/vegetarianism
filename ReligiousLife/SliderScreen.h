//
//  SplashScreen.h
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mushroom.h"
@interface SliderScreen : UIViewController<UIScrollViewDelegate>
{
    Mushroom *pageIndicator;
}
@property (strong, nonatomic) IBOutlet UIScrollView *welcomeScroll;
@property (nonatomic, strong) NSArray *imageArray;
@end
