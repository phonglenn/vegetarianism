//
//  ReferenceReligion.m
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "ReferenceReligion.h"
#import "SWRevealViewController.h"
#import "DetailRefReligion.h"
#import "AppDelegate.h"
#import "itemReference.h"

#define limitFetchRef 3
#define ITEM_ORIGIN_X 35
#define ITEM_WIDTH 110
#define ITEM_HEIGHT 100
#define ITEM_PADING 5
#define SEPARATOR_HEIGHT 7
@implementation ReferenceReligion
{
    BOOL loading;
}
@synthesize listRef;
@synthesize pageIndex;
@synthesize searchResultsArray;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return ([self.searchResultsArray count] +1 )/2;
        
    } else {
        return ([self.listRef count] +1 )/2;
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellReligion";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
//    Reference *ref = [self.listRef objectAtIndex:indexPath.row];
    CGRect frame = cell.frame;
    frame.size.height = 110;
    cell.frame = frame;
    
    Reference *ref1 = nil,*ref2= nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        ref1 = [searchResultsArray objectAtIndex:indexPath.row*2];
        if([searchResultsArray count] % 2 == 0)
        {
            ref2 = [searchResultsArray objectAtIndex:indexPath.row*2+1];
        }
        
    } else {
        ref1 = [listRef objectAtIndex:indexPath.row*2];
        if(indexPath.row*2 + 1 < [listRef count])
        {
            ref2 = [listRef objectAtIndex:indexPath.row*2+1];
        }
        
    }

    if(ref1 != nil)
    {
        itemReference *newItem1 = [[itemReference alloc] initWithFrame:CGRectMake(ITEM_ORIGIN_X, ITEM_PADING, ITEM_WIDTH, ITEM_HEIGHT)];
        [newItem1 setTitleAndImage:ref1.title image:nil tag:[ref1.idRef integerValue]];
        [newItem1.customButton addTarget:self action:@selector(itemReferenceClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:newItem1];
    }
    if(ref2 != nil)
    {
        itemReference *newItem2 = [[itemReference alloc] initWithFrame:CGRectMake(cell.frame.size.width - ITEM_ORIGIN_X - ITEM_WIDTH, ITEM_PADING, ITEM_WIDTH, ITEM_HEIGHT)];
        [newItem2 setTitleAndImage:ref2.title image:nil tag:[ref2.idRef integerValue]];
        [newItem2.customButton addTarget:self action:@selector(itemReferenceClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:newItem2];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    UIImageView *imagView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell-seperate-big"]];
    
    imagView.frame = CGRectMake(cell.textLabel.frame.origin.x + 5,cell.frame.size.height - 3.0, cell.frame.size.width - (cell.textLabel.frame.origin.x + 5)*2, SEPARATOR_HEIGHT);
    [cell.contentView addSubview:imagView];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)viewDidLoad
{
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    _pagingScroll = YES;
    
    // pull to refresh
    _topPullArrow = [[MPTopPullScroll alloc] initHeaderWithPullTitle:PULL_TO_REFRESH
                                                        releaseTitle:RELEASE_TO_REFRESH];
    [_topPullArrow setY:-(_topPullArrow.frame.size.height)];
}
-(void)viewDidAppear:(BOOL)animated
{
    // update reference from server
    [self requestReferenceFromServer];
}
-(void)viewDidDisappear:(BOOL)animated
{
    pageIndex = 0;
    self.listRef = nil;
    self.searchResultsArray = nil;
}
-(void)viewWillAppear:(BOOL)animated
{
    // load local reference
    pageIndex = 0;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.listRef = [NSMutableArray arrayWithArray:[appDelegate.dataHelper getObjectInTable:@"Reference" fromOffset:(pageIndex * limitFetchRef) limit:limitFetchRef]];
    
    // set title and navigation bar
    self.title = NSLocalizedString(@"Tư liệu tôn giáo",nil);
    
    UIImage *menuImage=nil;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)
    {
        menuImage = [UIImage imageNamed:@"menu-icon"];
    }
    else
    {
        menuImage = [[UIImage imageNamed:@"menu-icon"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:menuImage style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = menuButton;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background-wood"]]];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
#pragma mark - Item Reference Click

-(void)itemReferenceClick:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if(button.tag)
    {
        //
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                       initWithTitle:NSLocalizedString(@"Back", nil)
                                       style:UIBarButtonItemStylePlain
                                       target:nil
                                       action:nil];
        self.navigationItem.backBarButtonItem = backButton;
        
        DetailRefReligion *detailRef = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailRefReligion"];
        [self.navigationController pushViewController:detailRef animated:YES];
    }
}

#pragma Filter - Display Search Bar
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    searchResultsArray = [listRef filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark - Load database
- (void) loadMoreReference
{
    pageIndex++;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *loadMoreRef = [NSArray arrayWithArray:[appDelegate.dataHelper getObjectInTable:@"Reference" fromOffset:(pageIndex * limitFetchRef) limit:limitFetchRef]];
    for(Reference *ref in loadMoreRef)
    {
        // need to sort result
        [self.listRef addObject:ref];
    }
    [self.tableView reloadData];
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:([self.listRef count]-1) inSection:0] atScrollPosition:(UITableViewScrollPositionTop) animated:NO];

}
#pragma mark - REQUEST REFERENCE API
-(void)requestReferenceFromServer
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.hasNetwork)
    {
        [self showLoadingView];
        [appDelegate.dataCenter getListOfAllReferenceWithOffset:appDelegate.pageIndexRef limit:4 successBlock:^(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset)
         {
             [self hideLoadingView];
             if([items count] >0)
             {
                 appDelegate.pageIndexRef++;
                 pageIndex = 0;
                 [appDelegate.dataHelper saveContext];
                 // need to sort result
                 self.listRef = [NSMutableArray arrayWithArray:[appDelegate.dataHelper getObjectInTable:@"Reference" fromOffset:(pageIndex * limitFetchRef) limit:limitFetchRef]];
                 [self.tableView reloadData];
             }
             
         }errorBlock:^(id error) {
             [self hideLoadingView];
             NSLog(@"request failed");
         }];
    }
}
#pragma mark - HANDLE REFRESH DATA
- (void)handleReleaseAction
{
    [self requestReferenceFromServer];
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // UITableView only moves in one direction, y axis
    if (!_pagingScroll) return;
    _resetOffset = NO;
    
    if (_released) {
        [self handleReleaseAction];
        return;
    }
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    //NSInteger result = maximumOffset - currentOffset;
    
    // Change 10.0 to adjust the distance from bottom
    if (maximumOffset - currentOffset <= 10.0) {
        [self loadMoreReference];
        //[self methodThatAddsDataAndReloadsTableView];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_pagingScroll)
        return;
    _offset = scrollView.contentOffset.y;
    _released = FALSE;
    
    if (_offset < -64)
    {
        if (![_topPullArrow superview]) {
            [scrollView addSubview:_topPullArrow];
        }
        
        if (abs(_offset) < kScrollLimit) {
            [_topPullArrow holdScroll];
        } else {
            [_topPullArrow releaseScroll];
            _released = TRUE;
        }
    } else {
        if (_topPullArrow && [_topPullArrow superview]) {
            [_topPullArrow removeFromSuperview];
        }
    }
}


#pragma mark - MBProgressHUB

- (void)showLoadingViewWithMessage:(NSString *)message {
    if (!_progressView) {
        
        CGFloat x = (self.view.bounds.size.width - kLoadingViewWidth) / 2;
        CGFloat y = (self.view.bounds.size.height - kLoadingViewHeight) / 2;
        CGRect rect = CGRectMake(x, y, kLoadingViewWidth, kLoadingViewHeight);
        
        
        self.progressView = [[ProgressView alloc] initWithFrame:rect];
        [_progressView setTextColor:[UIColor whiteColor]];
        [_progressView setActivityColor:UIColorFromRGB(0x41AEC8)];
        _progressView.activityView.center = CGPointMake(_progressView.frame.size.width / 2, _progressView.frame.size.height / 2);
        _progressView.backgroundColor = [UIColor clearColor];
        [_progressView setBackgroundImage:nil];
    }
    
    [_progressView setTitle:message];
    [_progressView bringSubviewToFront:self.view];
    [_progressView showOnView:self.view animation:YES freeze:YES top:IS_IPAD ? self.navigationController.navigationBar.frame.size.height : 0];
}

- (void)showLoadingView {
    [self showLoadingViewWithMessage:@"Updating..."];
    loading = YES;
}

- (void)hideLoadingView {
    [_progressView performSelector:@selector(hide) onThread:[NSThread mainThread] withObject:nil waitUntilDone:YES];
    loading = NO;
}

@end
