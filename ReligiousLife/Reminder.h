//
//  Reminder.h
//  ReligiousLife
//
//  Created by harveynash on 3/25/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Reminder : NSManagedObject
@property(nonatomic, strong) NSDate *idRem;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *content;
@property(nonatomic, strong) NSDate *timeReminder;

+ (id)newReminder;

+ (id)reminderFromDatabaseForId:(NSDate *)itemId;

+ (id)addNewReminder:(Reminder *)rem;

@end
