//
//  Item.h
//  Kleii
//
//  Created by Khoai Nguyen on 7/10/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Reference : NSManagedObject

@property(nonatomic, strong) NSString *idRef;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *content;
@property(nonatomic, strong) NSString *category;
@property(nonatomic, strong) NSDate *lastUpdate;


+ (id)newRefernce;

+ (id)referenceFromDatabaseForId:(NSString *)itemId;

+ (id)referenceFromDictionary:(NSDictionary *)dict;

@end
