//
//  Vegetable.m
//  ReligiousLife
//
//  Created by harveynash on 3/25/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "Vegetable.h"
#import "AppDelegate.h"

@implementation Vegetable
@dynamic idVeg;
@dynamic title;
@dynamic content;
@dynamic lastUpdate;

+ (id)newVegetable {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return [appDelegate.dataHelper insertEntityWithEntityName:@"Vegetable"];
}

+ (id)vegetableFromDatabaseForId:(NSString *)vegId {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    Vegetable *veg = nil;
    if (vegId.length > 0) {
        veg = [appDelegate.dataHelper getVegetableForId:vegId];
    }
    return veg;
}
+ (id)vegetableFromDictionary:(NSDictionary *)dict {
    Vegetable *veg;
    if (dict.allKeys.count > 0) {
        
        /* try to get existing item */
        NSString *refId = [dict valueForKey:@"id"];
        veg = [Vegetable vegetableFromDatabaseForId:refId];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate* serverDate = [dateFormatter dateFromString:[dict valueForKey:@"date_created"]];
        if(veg)
        {
            if(veg.lastUpdate == serverDate)
            {
                return veg;
            }
        }
        else
        {
            veg = [Vegetable newVegetable];
        }
        veg.idVeg = [dict valueForKey:@"id"];
        veg.title = [dict valueForKey:@"title"];
        veg.content = [dict valueForKey:@"content"];
        veg.lastUpdate = serverDate;
    }
    return veg;
}
@end
