//
//  ReferenceReligion.h
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressView.h"
#import "MPTopPullScroll.h"
@interface ReferenceReligion : UITableViewController<ProgressViewDelegate,UIScrollViewDelegate>
{
    MPTopPullScroll        *_topPullArrow;
    int                    _offset;
    BOOL                   _resetOffset;
    BOOL                   _pagingScroll;
    BOOL                   _released;
    BOOL                   _selectMode;
}
@property(nonatomic, strong) ProgressView *progressView;
@property(nonatomic, strong) NSMutableArray *listRef;
@property(nonatomic, strong) NSMutableArray *searchResultsArray;
@property(nonatomic) int pageIndex;
@end
