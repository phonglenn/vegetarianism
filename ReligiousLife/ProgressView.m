/*============================================================================
 PROJECT: TalkWrench
 FILE:    ProgressView.m
 AUTHOR:  Nguyen Minh Khoai
 DATE:    2/22/13
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/
#import "ProgressView.h"

/*============================================================================
 PRIVATE MACRO
 =============================================================================*/
/*============================================================================
 PRIVATE INTERFACE
 =============================================================================*/

@implementation ProgressView
@synthesize activityView, titleLabel, cancelButton;
@synthesize delegate;
@synthesize overlayView;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        self.backgroundColor = [UIColor clearColor];
        _animationType = kProgressViewAnimationTypeFade;
        LayerConer(self.layer);

        /* create and add background view */
        imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        imageView.backgroundColor = [UIColor blackColor];
        imageView.alpha = 0.6f;
        imageView.layer.opaque = NO;
        [self addSubview:imageView];

        /* create and add activity view */
        CGFloat dy = (self.bounds.size.height - 20) / 2;
        activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityView.hidesWhenStopped = YES;
        [activityView setColor:UIColorFromRGB(0x000000)];
        [self addSubview:activityView];

        /* create and add label title */
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, dy, self.bounds.size.width - 30, self.bounds.size.height - 2 * dy)];
        titleLabel.font = FontBellWithSize(14);
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:titleLabel];

        /* create and add cancel button */
        cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelButton setFrame:CGRectMake(self.bounds.size.width - 30, dy, 20, 20)];
        [cancelButton addTarget:self action:@selector(didTouchedOnCancelButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelButton];
    }
    return self;
}

- (void)didTouchedOnCancelButton:(id)sender {
    CallTarget(delegate, @selector(didCanceledActionView:), self);
    [self hide];
}

- (void)setActivityColor:(UIColor *)color {
    [activityView setColor:color];
}

- (void)setTextColor:(UIColor *)color {
    [titleLabel setTextColor:color];
}

- (void)setBackgroundImage:(UIImage *)image {
    if (image) {
        imageView.image = image;
        imageView.alpha = 1.0;
    }
    imageView.backgroundColor = [UIColor clearColor];
}

- (void)setTitle:(NSString *)title {
    [titleLabel setText:title];
}

- (void)show:(BOOL)animated {
    if (visible) {
        return;
    }
    visible = YES;

    if (!self.superview) {
        UIWindow *topWindow = [UIApplication sharedApplication].keyWindow;

        /* add overlay view first */
        self.overlayView = [[UIView alloc] initWithFrame:kScreenFrame];
        overlayView.backgroundColor = [UIColor clearColor];
        [topWindow addSubview:overlayView];

        /* add main view now */
        [topWindow addSubview:self];
    }

    /* layout center */
    CGRect rect = self.frame;
    rect.origin.x = (kScreenFrame.size.width - rect.size.width) / 2;
    rect.origin.y = (kScreenFrame.size.height - rect.size.height) / 2;
    self.frame = rect;

    [activityView startAnimating];

    if (animated) {
        if (_animationType == kProgressViewAnimationTypeFade) {
            /* Show with effect */
            self.layer.opacity = 0;

            [UIView animateWithDuration:0.4f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.layer.opacity = 1;
                             }
                             completion:^(BOOL finished) {

                             }];

            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
            [UIView animateWithDuration:0.2 delay:0
                                options:UIViewAnimationCurveEaseOut
                             animations:^{
                                 self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                             }
                             completion:^(BOOL finished) {
                             }];
        }
    } else {
        self.layer.opacity = 1;
    }
}

- (void)showOnView:(UIView *)view animation:(BOOL)animated freeze:(BOOL)freeze top:(CGFloat)topMargin {
    if (visible) {
        return;
    }
    visible = YES;

    if (!self.superview) {
        if (freeze) {
            /* add overlay view first */
            self.overlayView = [[UIView alloc] initWithFrame:view.frame];
            overlayView.backgroundColor = [UIColor clearColor];
            overlayView.opaque = NO;
            CGRect rect = overlayView.frame;
            rect.origin = CGPointMake(0, 0 + topMargin);
            overlayView.frame = rect;

            [view addSubview:overlayView];
        }
        [view addSubview:self];
    }

    /* layout center */
    CGRect rect = self.frame;
    rect.origin.x = (view.frame.size.width - rect.size.width) / 2;
    rect.origin.y = (view.frame.size.height - rect.size.height) / 2;
    self.frame = rect;

    [activityView startAnimating];

    if (animated) {
        if (_animationType == kProgressViewAnimationTypeFade) {
            /* Show with effect */
            self.layer.opacity = 0;

            [UIView animateWithDuration:0.4f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.layer.opacity = 1;
                             }
                             completion:^(BOOL finished) {

                             }];

            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
            [UIView animateWithDuration:0.2 delay:0
                                options:UIViewAnimationCurveEaseOut
                             animations:^{
                                 self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                             }
                             completion:^(BOOL finished) {
                             }];
        }
    } else {
        self.layer.opacity = 1;
    }
}

- (void)showOnView:(UIView *)view animation:(BOOL)animated {
    if (visible) {
        return;
    }
    visible = YES;

    if (!self.superview) {
        [view addSubview:self];
    }

    /* layout center */
    CGRect rect = self.frame;
    rect.origin.x = (view.frame.size.width - rect.size.width) / 2;
    rect.origin.y = (view.frame.size.height - rect.size.height) / 2;
    self.frame = rect;

    [activityView startAnimating];

    if (animated) {
        if (_animationType == kProgressViewAnimationTypeFade) {
            /* Show with effect */
            self.layer.opacity = 0;

            [UIView animateWithDuration:0.4f
                                  delay:0.0f
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^{
                                 self.layer.opacity = 1;
                             }
                             completion:^(BOOL finished) {

                             }];

            self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
            [UIView animateWithDuration:0.2 delay:0
                                options:UIViewAnimationCurveEaseOut
                             animations:^{
                                 self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
                             }
                             completion:^(BOOL finished) {
                             }];
        }
    } else {
        self.layer.opacity = 1;
    }
}

- (void)show {
    [self show:YES];
}


- (void)clear {
    /* stop activity */
    [activityView stopAnimating];

    /* remove overlay view */
    [overlayView removeFromSuperview];

    /* remove view from its parent */
    [self removeFromSuperview];

    [activityView stopAnimating];
}

- (void)hide:(BOOL)animated {
    if (!visible) {
        return;
    }
    visible = NO;

    if (self.superview) {
        if (animated) {
            if (_animationType == kProgressViewAnimationTypeFade) {
                /* zoom in animation */
                [UIView animateWithDuration:0.3f
                                 animations:^{
                                     self.layer.opacity = 0;
                                 }
                                 completion:^(BOOL finished) {
                                     [self clear];
                                 }];
            }
        } else {
            [self clear];
        }
    }

}

- (void)hide {
    [self hide:YES];
}

@end
