/*============================================================================
 PROJECT: Halalgems
 FILE:    DataCenter.m
 AUTHOR:  Nguyen Minh Khoai
 =============================================================================*/

/*============================================================================
 IMPORT
 =============================================================================*/

#import "BaseNetwork.h"
#import "JSONHelper.h"


/*----------------------------------------------------------------------------
 Interface:   DataCenter
 -----------------------------------------------------------------------------*/
@interface DataCenter () {
}



@end

@implementation DataCenter


- (id)init {
    self = [super init];
    if (self) {
        /* load previous user information */
        
    }
    return self;
}

- (void)getListOfAllReferenceWithOffset:(NSInteger)fileOffset
                              limit:(NSInteger)limit
                       successBlock:(void (^)(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset))successBlock
                         errorBlock:(void (^)(id))errorBlock {
    
    /* create parameters */
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:[NSString stringWithFormat:@"%d", fileOffset] forKey:@"pageIndex"];
    [params setValue:[NSString stringWithFormat:@"%d", limit] forKey:@"pageSize"];
    [params setValue:@"1" forKey:@"category_id"];

    
    /* call webservice */
    [self callGETServiceWithURL:@"http://vegetable.vocit.net/index.php/reference/listRefence"
                         params:params
                   successBlock:^(id response) {
                       [JSONHelper getListOfReferenceFromData:response
                                               onCompleteBlock:^(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset) {
                                                   successBlock(items, fileTotal, fileOffset);
                                               }];
                   } errorBlock:^(id error) {
                       errorBlock(error);
                   }];
}

- (void)getListOfAllVegetableWithOffset:(NSInteger)fileOffset
                                  limit:(NSInteger)limit
                           successBlock:(void (^)(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset))successBlock
                             errorBlock:(void (^)(id))errorBlock {
    
    /* create parameters */
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:[NSString stringWithFormat:@"%d", fileOffset] forKey:@"pageIndex"];
    [params setValue:[NSString stringWithFormat:@"%d", limit] forKey:@"pageSize"];
    [params setValue:@"2" forKey:@"category_id"];
    
    /* call webservice */
    [self callGETServiceWithURL:@"http://vegetable.vocit.net/index.php/reference/listRefence"
                         params:params
                   successBlock:^(id response) {
                       [JSONHelper getListOfVegetableFromData:response
                                               onCompleteBlock:^(NSMutableArray *items, NSInteger fileTotal, NSInteger fileOffset) {
                                                   successBlock(items, fileTotal, fileOffset);
                                               }];
                   } errorBlock:^(id error) {
                       errorBlock(error);
                   }];
}
/*----------------------------------------------------------------------------
 Method:      callGETServiceWithURL
 Description: call a get request with url and parameters
 ----------------------------------------------------------------------------*/
- (void)callGETServiceWithURL:(NSString *)url
                       params:(NSMutableDictionary *)params
                 successBlock:(void (^)(id))successBlock
                   errorBlock:(void (^)(id))errorBlock {
    
    [self callServiceWithURL:url
                      params:params
                  httpMethod:@"POST"
                successBlock:^(id dataJson) {
                    successBlock(dataJson);
                }
                  errorBlock:^(id dataJson) {
                      errorBlock(dataJson);
                  }];
}
#pragma mark - Support Normal Request
/*----------------------------------------------------------------------------
 Method:      callServiceWithURL
 Description: call a service with url, parameters and method name
 ----------------------------------------------------------------------------*/
- (void)callServiceWithURL:(NSString *)url
                    params:(NSMutableDictionary *)params
                httpMethod:(NSString *)method
              successBlock:(void (^)(id))successBlock
                errorBlock:(void (^)(id))errorBlock {
    if (!params) {
        params = [NSMutableDictionary new];
    }
    
    /* call service with parameter */
    [[BaseNetwork service] requestWithURL:url
                                   params:params
                             successBlock:^(NSDictionary *data) {
                                 successBlock(data);
                             }
                               errorBlock:^(NSString *error) {
                                   /* handle error */
                                   NSString *errorMsg = kServiceNotAvailableMessage;
                                   NSDictionary *dict = [error JSONValue];
                                   if (dict) {
                                       NSArray *errors = [[dict valueForKey:@"result"] valueForKey:@"errors"];
                                       if (errors.count > 0) {

                                       }
                                   }
                                   errorBlock(errorMsg);
                               }
                               httpMethod:method];
}

@end