//
//  CCCoreDataHelper.h
//  CoreDate&iCloud
//
//  Created by Bao Nhan on 11/13/12.
//  Copyright (c) 2012 Nexle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataSystem.h"
#import "Reference.h"
#import "Reminder.h"
#import "Vegetable.h"
typedef NS_ENUM(NSInteger, SortedEntityByType) {
    kSortedEntityByName,
    kSortedEntityByDate
};

@interface CCCoreDataHelper : CoreDataSystem


/* For Reference */
- (Reference *)getReferenceForId:(NSString *)refId;

- (NSArray *)getObjectInTable:(NSString *)table fromOffset:(int)offset limit:(int)limit;

/* For Reminder */
- (Reminder *)getReminderForId:(NSDate *)remId;

/* For Vegetable */
- (Vegetable *)getVegetableForId:(NSString *)vegId;

- (NSArray *)getAvailableFavoriteFiles;


- (void)clearAllData;



@end
