//
//  CCCoreDataHelper.m
//  CoreDate&iCloud
//
//  Created by Bao Nhan on 11/13/12.
//  Copyright (c) 2012 Nexle. All rights reserved.
//

#import "CCCoreDataHelper.h"


typedef enum {
    kCompoundPredicateTypeAnd,
    kCompoundPredicateTypeOr,
    kCompoundPredicateTypeFetchLimit
} CompoundPredicateType;

#define kOperationKey @"kOperationKey"
#define kFieldKey @"kFieldKey"
#define kValueKey @"kValueKey"
#define kSortKey @"kSortKey"
#define kPageIndex @"kPageIndex"

static NSDictionary *operationDictionary(NSString *field, NSString *operation, NSString *value) {
    return @{kFieldKey : field, kOperationKey : operation, kValueKey : value};
}

static NSDictionary *operationReminder(NSString *field, NSString *operation, NSDate *value) {
    return @{kFieldKey : field, kOperationKey : operation, kValueKey : value};
}

static NSDictionary *sortDictionary(NSString *field, NSString *operation) {
    return @{kFieldKey : field, kSortKey : operation};
}

@implementation CCCoreDataHelper

#pragma mark - Support Methods

- (void)deleteObject:(NSManagedObject *)object {
    [self.managedObjectContext deleteObject:object];
    [self saveContext];
}

- (NSFetchRequest *)createFetchRequestForEntity:(NSString *)entityName inContext:(NSManagedObjectContext *)context {
    /* create entity desciption */
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    NSFetchRequest *fetchReqest = [[NSFetchRequest alloc] init];
    [fetchReqest setEntity:entity];
    return fetchReqest;
}

- (NSFetchRequest *)createFetchRequestInTable:(NSString *)table
                                   parameters:(NSArray *)parameters
                                    queryType:(CompoundPredicateType)type {
    @autoreleasepool {
        /* get current context */
        NSManagedObjectContext *context = [self managedObjectContext];

        /* create fetch request */
        NSFetchRequest *fetchRequest = [self createFetchRequestForEntity:table inContext:context];
        NSMutableArray *parr = [NSMutableArray array];
        NSMutableArray *sortArr = [NSMutableArray array];
        for (NSDictionary *dict in parameters) {

            /* format operation */
            if (dict[kOperationKey]) {
                NSString *formatString = @"";
                if ([dict[kOperationKey] isEqualToString:@"="]) {
                    formatString = @"%K = %@";
                } else if ([dict[kOperationKey] isEqualToString:@"!="]) {
                    formatString = @"%K != %@";
                }

                [parr addObject:[NSPredicate predicateWithFormat:formatString, dict[kFieldKey], dict[kValueKey]]];
            }

            /* sort operation */
            if (dict[kSortKey]) {
                BOOL ascending = [dict[kSortKey] isEqualToString:@"asc"];
                NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:dict[kFieldKey] ascending:ascending];
                [sortArr addObject:descriptor];
            }
            
        }

        /* swith to corresponding types */
        switch (type) {
            case kCompoundPredicateTypeAnd: {
                NSPredicate *andPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:parr];
                [fetchRequest setPredicate:andPredicate];
            }
                break;
            default:
                break;
        }

        /* filter by sort */
        if (sortArr.count > 0) {
            [fetchRequest setSortDescriptors:sortArr];
        }

        [fetchRequest shouldRefreshRefetchedObjects];
        return fetchRequest;
    }
}

- (NSArray *)getObjectsInTable:(NSString *)table parameters:(NSArray *)parameters {
    /* create fetch request */
    NSFetchRequest *fetchRequest = [self createFetchRequestInTable:table
                                                        parameters:parameters
                                                         queryType:kCompoundPredicateTypeAnd];
    /* get current context */
    NSManagedObjectContext *context = [self managedObjectContext];
    

    /* get object information */
    return [context executeFetchRequest:fetchRequest error:nil];
}


- (id)getObjectInTable:(NSString *)table parameters:(NSArray *)parameters {
    /* get object information */
    id result = nil;
    NSArray *objects = [self getObjectsInTable:table parameters:parameters];
    for (NSManagedObject *object in objects) {
        result = object;
        break;
    }
    return result;
}
- (NSFetchRequest *)createFetchRequestInTable:(NSString *)table
                                   fromOffset:(int )offset
                                    limit:(int)limit {
        /* get current context */
        NSManagedObjectContext *context = [self managedObjectContext];
        
        /* create fetch request */
        NSFetchRequest *fetchRequest = [self createFetchRequestForEntity:table inContext:context];
        fetchRequest.fetchOffset = offset;
        fetchRequest.fetchLimit = limit;
    
        NSMutableArray *sortArr = [NSMutableArray array];
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"lastUpdate" ascending:YES];
    if(![table isEqualToString:@"ListReminder"])
    {
        [sortArr addObject:descriptor];
    }
    
        if (sortArr.count > 0) {
            [fetchRequest setSortDescriptors:sortArr];
        }
    
        [fetchRequest shouldRefreshRefetchedObjects];
    
    return fetchRequest;
}

- (NSArray *)getObjectsInTable:(NSString *)table fromOffset:(int)offset limit:(int)limit {
    /* create fetch request */
    NSFetchRequest *fetchRequest = [self createFetchRequestInTable:table
                                                        fromOffset:offset limit:limit];
    /* get current context */
    NSManagedObjectContext *context = [self managedObjectContext];
    
    
    /* get object information */
    return [context executeFetchRequest:fetchRequest error:nil];
}
- (NSArray *)getObjectInTable:(NSString *)table fromOffset:(int)offset limit:(int)limit{
    /* get object information */
    NSArray *objects = [self getObjectsInTable:table fromOffset:offset limit:limit];
    return objects;
}

- (NSArray *)getObjectsFromTable:(NSString *)name {
    /* get current context */
    NSManagedObjectContext *context = [self managedObjectContext];

    /* create fetch request */
    NSFetchRequest *fetchRequest = [self createFetchRequestForEntity:name inContext:context];

    NSArray *objects = [context executeFetchRequest:fetchRequest error:nil];
    return (objects.lastObject ? objects : nil);
}

- (NSInteger)countChildrenInTable:(NSString *)table parameters:(NSArray *)parameters {
    /* create fetch request */
    NSFetchRequest *fetchRequest = [self createFetchRequestInTable:table
                                                        parameters:parameters
                                                         queryType:kCompoundPredicateTypeAnd];
    /* get current context */
    NSManagedObjectContext *context = [self managedObjectContext];

    NSError *error = nil;
    NSInteger count = [context countForFetchRequest:fetchRequest error:&error];
    if (error) {
        count = 0;
    }
    return count;
}

- (id)specificationValueInTable:(NSString *)table
                     parameters:(NSArray *)parameters
                      operation:(NSDictionary *)operation {

    /* get all objects matching with request */
    NSArray *objs = [self getObjectsInTable:table parameters:parameters];

    if (objs.count > 0) {
        NSString *key = [operation.allKeys objectAtIndex:0];

        NSString *sqlString = [NSString stringWithFormat:@"@%@.%@", key, [operation valueForKey:key]];
        return [objs valueForKeyPath:sqlString];
    }
    return nil;
}

#pragma mark - Reference Helper Methods

- (Reference *)getReferenceForId:(NSString *)refId {
    if (refId.length > 0) {
        return (Reference *) [self getObjectInTable:@"Reference" parameters:@[operationDictionary(@"idRef", @"=", refId)]];
    }
    return nil;
}

- (NSArray *)getReferenceFromOffset:(int )offset limit:(int)limit{
    if (offset > 0) {
        return (NSArray *) [self getObjectInTable:@"Reference" fromOffset:offset limit:limit];
    }
    return nil;
}


- (NSArray *)getAvailableFavoriteFiles {
    NSFetchRequest *request = [self.managedObjectModel fetchRequestTemplateForName:@"GetAllFavoriteFiles"];
    NSError *error;
    NSArray *results = [self.managedObjectContext executeFetchRequest:request error:&error];
    return results;
}

#pragma mark - Reminder Helper Methods

- (Reminder *)getReminderForId:(NSDate *)remId {
    if (remId) {
        return (Reminder *) [self getObjectInTable:@"ListReminder" parameters:@[operationReminder(@"idRem", @"=", remId)]];
    }
    return nil;
}

#pragma mark - Reminder Helper Methods

- (Vegetable *)getVegetableForId:(NSString *)vegId {
    if (vegId.length > 0) {
        return (Vegetable *) [self getObjectInTable:@"Vegetable" parameters:@[operationDictionary(@"idVeg", @"=", vegId)]];
    }
    return nil;
}

- (void)clearAllData {
    [self.managedObjectContext reset];
    self.managedObjectContext = nil;
    self.managedObjectModel = nil;
    
    NSArray *stores = [self.persistentStoreCoordinator persistentStores];
    
    for (NSPersistentStore *store in stores) {
        [self.persistentStoreCoordinator removePersistentStore:store error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:nil];
    }
    
    self.persistentStoreCoordinator = nil;
}

@end
