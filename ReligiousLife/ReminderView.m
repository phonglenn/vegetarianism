//
//  ReminderView.m
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import "ReminderView.h"

@implementation ReminderView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellReminder";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = @"Reminder";
    cell.imageView.image = [UIImage imageNamed:@"cellImage"];
    return cell;
}


@end
