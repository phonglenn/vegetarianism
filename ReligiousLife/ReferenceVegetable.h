//
//  ReferenceVegetable.h
//  ReligiousLife
//
//  Created by harveynash on 3/11/14.
//  Copyright (c) 2014 com.appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPTopPullScroll.h"
#import "ProgressView.h"
@interface ReferenceVegetable : UITableViewController<UIScrollViewDelegate>
{
    MPTopPullScroll        *_topPullArrow;
    int                    _offset;
    BOOL                   _resetOffset;
    BOOL                   _pagingScroll;
    BOOL                   _released;
    BOOL                   _selectMode;
}
@property(nonatomic, strong) ProgressView *progressView;
@property(nonatomic, strong) NSMutableArray *vegetableRefArray;
@property(nonatomic, strong) NSMutableArray *searchResultsArray;
@property(nonatomic) int pageIndex;
@end
